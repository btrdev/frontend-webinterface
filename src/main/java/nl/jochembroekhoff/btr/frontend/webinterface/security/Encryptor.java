package nl.jochembroekhoff.btr.frontend.webinterface.security;

/**
 * Source: https://stackoverflow.com/a/22445878/5003260 . Met enkenle
 * wijzigingen.
 *
 * @author Chand Priyankara
 * @author Jochem Broekhoff
 */
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import nl.jochembroekhoff.btr.api.BtrApi;

import org.apache.commons.codec.binary.Base64;

public class Encryptor {

    public static String encrypt(String key, String initVector, Serializable value) {
        try {
            return encrypt(key, initVector, BtrApi.serializableToBytes(value, true));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String encrypt(String key, String initVector, byte[] value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value);

            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static <T extends Serializable> T decrypt(String key, String initVector, Class<T> type, String encrypted) throws IllegalBlockSizeException, BadPaddingException, IOException, ClassNotFoundException, IOException {
        byte[] decrypted = decrypt(key, initVector, encrypted);
        return BtrApi.bytesToSerializable(decrypted, type, true);
    }

    public static byte[] decrypt(String key, String initVector, String encrypted) throws IllegalBlockSizeException, BadPaddingException {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            return original;
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
