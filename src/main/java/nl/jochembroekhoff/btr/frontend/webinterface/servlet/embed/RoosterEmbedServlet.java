/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet.embed;

import com.google.appengine.api.datastore.Entity;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.LogIn;
import nl.jochembroekhoff.btr.api.cup2.deel.PrintbaarRooster;
import nl.jochembroekhoff.btr.api.cup2.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.PrintbaarRoosterResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ZoekNamenResultaat;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.CupTools;
import nl.jochembroekhoff.btr.frontend.webinterface.RoosterTools;
import nl.jochembroekhoff.btr.frontend.webinterface.security.Encryptor;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.IndexRooster;
import org.javatuples.Pair;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "RoosterEmbedServlet", urlPatterns = {"/embed/RoosterEmbedServlet"})
public class RoosterEmbedServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String resetIndex = request.getParameter("resetIndex");

            String code = request.getParameter("code");
            String type = request.getParameter("type");
            String locatie = request.getParameter("locatie");
            String week = request.getParameter("week");

            if (resetIndex != null) {
                //
                // Rooster index resetten
                //
                IndexRooster.resetIndex();
            } else if (code != null && type != null && locatie != null && week != null) {
                //
                // Rooster ophalen
                //

                //Maak een index van het rooster indien nodig
                IndexRooster.indexRooster();

                if (week.length() == 1) {
                    week = "0" + week; //legacy support, ooit nog eens weghalen
                }
                Week laadWeek = null;
                for (Week w : ApiRooster.INDEXERINGEN.get(locatie).getWeken()) {
                    if (w.getIdentificator().equals(week)) {
                        laadWeek = w;
                    }
                }

                if (laadWeek == null) {
                    out.println("<i>Geselecteerde week (" + week + ") kon niet worden geladen.</i>");
                } else {
                    PrintbaarRoosterResultaat prr = null;

                    String clientKey = request.getHeader("Client-Key");
                    String bewaartoken = request.getHeader("Bewaartoken");
                    if (clientKey != null && bewaartoken != null
                            && (clientKey.length() == 16 || clientKey.length() == 32)) {
                        try {
                            Pair<Entity, SessieData> sessie = CupTools.getSessie(clientKey, bewaartoken);
                            Entity entity = sessie.getValue0();
                            SessieData sessieData = sessie.getValue1();
                            boolean verder = false;
                            if ((boolean) entity.getProperty("inlogOK")) {
                                if (CupTools.checkVereistOpnieuwInloggen(entity)) {
                                    SessieData nieuweSessie = ApiCup2.getNieuweSessie();

                                    String iv = (String) entity.getProperty("iv");
                                    String gebruikersnaam = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("gebruikersnaam")));
                                    String pincode = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("pincode")));

                                    ZoekNamenResultaat znr = ZoekNamen.zoekNamen(nieuweSessie, (String) entity.getProperty("zoekletters"));
                                    if (znr.gelukt()) {
                                        LogInResultaat lir = LogIn.inloggen(znr.getSessieData(), gebruikersnaam, pincode);
                                        if (lir.gelukt()) {
                                            verder = true;
                                            sessieData = lir.getSessieData();

                                            CupTools.updateSessie(entity, "logIn", clientKey, sessieData);
                                        }
                                    }
                                } else {
                                    verder = true;
                                }
                            }

                            if (verder) {
                                //prr laden
                                prr = PrintbaarRooster.printbaarRooster(sessieData);
                                if (prr.gelukt()) {
                                    CupTools.updateSessie(entity, "printbaarRooster", clientKey, prr.getSessieData());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    LaadRoosterResultaat lrr = RoosterTools.laadRooster(code, type, laadWeek, locatie);
                    out.println(RoosterTools.renderLaadRoosterResultaat(lrr, prr, code, locatie, true, true));

                    /*
                    out.println("Headers<hr/>");
                    Enumeration<String> headerNames = request.getHeaderNames();
                    while (headerNames.hasMoreElements()) {
                        String headerName = headerNames.nextElement();
                        out.print("Header Name: <em>" + headerName);
                        String headerValue = request.getHeader(headerName);
                        out.print("</em>, Header Value: <em>" + headerValue);
                        out.println("</em><br/>");
                    }
                     */
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
