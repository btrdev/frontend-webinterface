/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet.api;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.LogIn;
import nl.jochembroekhoff.btr.api.cup2.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ZoekNamenResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.CupTools;
import nl.jochembroekhoff.btr.frontend.webinterface.security.Encryptor;
import nl.jochembroekhoff.btr.frontend.webinterface.security.RandomString;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.utils.URIBuilder;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "CupApiServlet", urlPatterns = {"/api/CupApiServlet"})
public class CupApiServlet extends HttpServlet {

    private static DatastoreService DS = DatastoreServiceFactory.getDatastoreService();
    RandomString ivGenerator = new RandomString(16);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            try {
                String clientKey = request.getHeader("Client-Key");
                if (clientKey == null) {
                    out.println("ERR|CLIENT_KEY_NULL");
                    return;
                } else if (clientKey.length() != 16 && clientKey.length() != 32) {
                    out.println("ERR|CLIENT_KEY_ONGELDIGE_LENGTE|" + clientKey.length());
                    return;
                }

                String actie = request.getParameter("actie");
                if (actie == null) {
                    String iv = ivGenerator.nextString();
                    SessieData sessieData = ApiCup2.getNieuweSessie();

                    Entity nieuweSessie = new Entity("Cup2Inloggegevens");
                    nieuweSessie.setProperty("iv", iv);
                    nieuweSessie.setProperty("sessieData", new Text(Encryptor.encrypt(
                            clientKey,
                            iv,
                            sessieData)));
                    nieuweSessie.setProperty("sessieLaatsteUpdate", new Date());
                    nieuweSessie.setProperty("zoekletters", "");
                    nieuweSessie.setProperty("gebruikersnaam", "");
                    nieuweSessie.setProperty("pincode", "");
                    nieuweSessie.setProperty("inlogOK", false);
                    nieuweSessie.setProperty("laatsteActie", "nieuweSessie");
                    DS.put(nieuweSessie);

                    out.println("BEWAARTOKEN|" + KeyFactory.keyToString(
                            nieuweSessie.getKey()));
                } else {
                    String bewaartoken = request.getHeader("Bewaartoken");
                    if (bewaartoken == null) {
                        out.println("ERR|BEWAARTOKEN_NULL");
                        return;
                    }

                    Key key = KeyFactory.stringToKey(bewaartoken);
                    Entity entity = DS.get(key);
                    if (entity == null) {
                        out.println("ERR|BEWAARTOKEN_ONGELDIG_OF_VERLOPEN");
                        return;
                    }

                    String iv = (String) entity.getProperty("iv");
                    SessieData sessieData = Encryptor.decrypt(
                            clientKey,
                            iv,
                            SessieData.class,
                            ((Text) entity.getProperty("sessieData")).getValue());
                    String laatsteActie = (String) entity.getProperty("laatsteActie");
                    boolean inlogOK = (boolean) entity.getProperty("inlogOK");

                    if (inlogOK) {
                        boolean nieuweInlogOK = false;
                        if (CupTools.checkVereistOpnieuwInloggen(entity)) {
                            System.out.println("[CUP2]: Nieuwe inlog beginnen...");

                            SessieData nieuweSessie = ApiCup2.getNieuweSessie();
                            ZoekNamenResultaat znr = ZoekNamen.zoekNamen(nieuweSessie, (String) entity.getProperty("zoekletters"));
                            if (znr.gelukt()) {
                                String gebruikersnaam = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("gebruikersnaam")));
                                String pincode = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("pincode")));

                                LogInResultaat lir = LogIn.inloggen(znr.getSessieData(), gebruikersnaam, pincode);
                                if (lir.gelukt()) {
                                    nieuweInlogOK = true;
                                    sessieData = lir.getSessieData();
                                }
                            }

                            if (!nieuweInlogOK) {
                                out.println("ERR|OPNIEUW_INLOGGEN");
                                return;
                            }
                        }
                    }
                    switch (actie) {
                        case "zoekNamen":
                            if (checkActieVerplichtingen(laatsteActie, out, "nieuweSessie", "zoekNamen")) {
                                return;
                            }

                            String zoekletters = request.getParameter("zoekletters");
                            if (zoekletters == null) {
                                out.println("ERR|ZOEKLETTERS_NULL");
                                return;
                            } else if (zoekletters.length() < 3) {
                                out.println("ERR|ZOEKLETTERS_TE_KORT");
                                return;
                            }

                            ZoekNamenResultaat znr = ZoekNamen.zoekNamen(sessieData, zoekletters);

                            if (!znr.gelukt()) {
                                out.println("ERR|ONBEKEND|zoekNamen");
                            } else {
                                entity.setProperty("zoekletters", zoekletters);

                                znr.getNamen().forEach((k, v) -> {
                                    out.println(k + "|" + v);
                                });
                            }

                            CupTools.updateSessie(entity, "zoekNamen", clientKey, znr.getSessieData());
                            break;
                        case "logIn":
                            if (checkActieVerplichtingen(laatsteActie, out, "zoekNamen", "logIn")) {
                                return;
                            }

                            if (inlogOK) {
                                out.println("ERR|AL_INGELOGD");
                                return;
                            }

                            String gebruikersnaam = request.getParameter("gebruikersnaam");
                            String pincode = request.getParameter("pincode");
                            if (gebruikersnaam == null) {
                                out.println("ERR|GEBRUIKERSNAAM_NULL");
                                return;
                            }
                            if (pincode == null) {
                                out.println("ERR|PINCODE_NULL");
                                return;
                            }

                            LogInResultaat lir = LogIn.inloggen(sessieData, gebruikersnaam, pincode);
                            if (!lir.gelukt()) {
                                out.println("ERR|TEKST|logIn|" + lir.getFoutmelding());
                            } else {
                                entity.setProperty("inlogOK", true);
                                entity.setProperty("gebruikersnaam", Encryptor.encrypt(
                                        clientKey,
                                        iv,
                                        gebruikersnaam.getBytes()));
                                entity.setProperty("pincode", Encryptor.encrypt(
                                        clientKey,
                                        iv,
                                        pincode.getBytes()));
                                out.println("OK");
                            }

                            if (lir.getSessieData() != null) {
                                CupTools.updateSessie(entity, "logIn", clientKey, lir.getSessieData());
                            }
                            break;
                        case "contacturenLijst":
                            out.println("ERR|NIET_GEIMPLEMENTEERD");
                            break;
                        case "webPaginaURL":
                            SessieData eenmaligeSessie = CupTools.opnieuwInloggen(entity, clientKey);
                            if (eenmaligeSessie != null) {
                                out.println(new URIBuilder(ApiCup2.getCupBasisUri())
                                        .setPath(ApiCup2.getCupBasisUri().getPath()
                                                + "/" + eenmaligeSessie.getToken()
                                                + "/RoosterForm.aspx").build().toURL());
                                eenmaligeSessie.getCookies().forEach((cookieNaam, cookieWaarde) -> {
                                    out.println(cookieNaam + "|" + Base64.encodeBase64String(cookieWaarde.getBytes()));
                                });
                            } else {
                                out.println("ERR|TEKST|webPaginaURL|Webpagina-url mislukt aan te maken.");
                            }
                            break;
                        default:
                            out.println("ERR|ACTIE_ONBEKEND");
                            break;
                    }
                }
            } catch (Exception e) {
                out.println("ERR|" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("ERR|VEREIST_POST");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean checkActieVerplichtingen(String laatsteActie, PrintWriter out, String... verplichteActies) {
        boolean hit = false;
        for (String verplichteActie : verplichteActies) {
            if (laatsteActie.equals(verplichteActie)) {
                hit = true;
            }
        }

        if (!hit && verplichteActies.length > 0) {
            out.println("ERR|VERPLICHT_EERST_ACTIE|" + verplichteActies[0]);
        } else if (!hit) {
            out.println("ERR|VERPLICHT_EERST_ACTIE_INCORRECT");
        }

        return !hit;
    }

}
