/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Jochem
 */
public class HtmlTools {

    private static final Map<String, String> ASSETS = new HashMap<>();

    public static String getAsset(String path) {
        //if(ASSETS.containsKey(path)){
        return "./assets/" + path;
        //}
        //calculate sum
        //return "";
    }

    public static String getBasicHead(String title) {
        return "<meta charst=\"utf-8\">"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">"
                + "<title>" + title + " | BTRooster</title>"
                + "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css\" integrity=\"sha256-2k1KVsNPRXxZOsXQ8aqcZ9GOOwmJTMoOB5o5Qp1d6/s=\" crossorigin=\"anonymous\" />"
                + "<script defer src=\"https://use.fontawesome.com/releases/v5.0.6/js/all.js\"></script>";
    }

    public static String getBasicFooter() {
        return getBasicFooter(null);
    }

    public static String getBasicFooter(String paginaScript) {
        String optioneelScript = "";
        if (!StringUtils.isBlank(paginaScript)) {
            optioneelScript += "<script>btrVerbose= true;</script>"
                    + "<script src=\"" + getAsset("js/BTR.js") + "\"></script>"
                    + "<script src=\"" + getAsset("js/" + paginaScript + ".js") + "\"></script>";
        }
        return "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js\" integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\" crossorigin=\"anonymous\"></script>\n"
                + optioneelScript;
    }

    public static String getExtendedFooter() {
        return getBasicFooter("Overzicht");
    }
}
