/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.statics;

/**
 *
 * @author jochem
 */
public class Constanten {

    public static final String[] DAGEN_WEERGAVE = new String[]{
        "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag"
    };
    public static final String[] DAGEN_ALGEMEEN = new String[]{
        "Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"
    };
    public static final String[] MAANDAN = new String[]{
        "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"
    };
}
