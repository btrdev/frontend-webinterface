/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet.api;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import nl.jochembroekhoff.btr.api.rooster.container.Indexering;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.frontend.webinterface.RoosterTools;
import nl.jochembroekhoff.btr.frontend.webinterface.security.RandomString;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.AlgemeneParameter;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.IndexRooster;
import org.apache.commons.lang3.mutable.MutableBoolean;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "RoosterApiServlet", urlPatterns = {"/api/RoosterApiServlet"})
public class RoosterApiServlet extends HttpServlet {

    private static DatastoreService DS = DatastoreServiceFactory.getDatastoreService();
    RandomString ivGenerator = new RandomString(16);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            try {
                String actie = request.getParameter("actie");
                if (actie == null) {
                    out.println("ERR|ACTIE_NULL");
                } else {
                    //Zorg er voor dat het rooster geindexeerd is
                    IndexRooster.indexRooster();

                    switch (actie) {
                        case "codes": {
                            MutableBoolean check = new MutableBoolean(true);
                            String locatie = AlgemeneParameter
                                    .checkParameterWaarde(request, out, AlgemeneParameter.LOCATIE, check);
                            String type = AlgemeneParameter
                                    .checkParameterWaarde(request, out, AlgemeneParameter.TYPE, check);
                            if (!check.booleanValue()) {
                                return;
                            }

                            try {
                                Indexering index = ApiRooster.INDEXERINGEN.get(locatie);
                                Set<String> codes = null;
                                switch (type) {
                                    case "s":
                                        codes = index.getLeerlingen().keySet();
                                        break;
                                    case "t":
                                        codes = index.getDocenten().keySet();
                                        break;
                                    case "r":
                                        codes = index.getLokalen().keySet();
                                        break;
                                    case "c":
                                        codes = index.getKlassen().keySet();
                                        break;
                                }

                                codes.forEach(out::println);
                            } catch (NullPointerException npe) {
                                npe.printStackTrace();
                                out.println("ERR|NULL_POINTER");
                            }
                            break;
                        }
                        case "weken": {
                            MutableBoolean check = new MutableBoolean(true);
                            String locatie = AlgemeneParameter
                                    .checkParameterWaarde(request, out, AlgemeneParameter.LOCATIE, check);
                            if (!check.booleanValue()) {
                                return;
                            }

                            try {
                                Indexering index = ApiRooster.INDEXERINGEN.get(locatie);
                                index.getWeken().forEach(week -> {
                                    out.println(week.getIdentificator() + "|" + week.getDatum().toString(BtrApi.DT_FORMATTER_JAAR_MAAND_DAG));
                                });
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                out.println("ERR|NULL_POINTER");
                            }
                            break;
                        }
                        default:
                            out.println("ERR|ACTIE_ONBEKEND");
                            break;
                    }
                }
            } catch (Exception e) {
                out.println("ERR|" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
