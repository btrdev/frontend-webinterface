/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet.cron;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joda.time.DateTime;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "CleanupCache", urlPatterns = {"/cron/CleanupCache"})
public class CleanupCache extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("Start: CleanupCache<br/>");
            out.println("Query 1: Lesrooster<br/>");
            DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
            try {
                Query query = new Query("Lesrooster");
                Query.Filter lastupdateFilter = new Query.FilterPredicate("lastupdate", Query.FilterOperator.LESS_THAN, DateTime.now().minusWeeks(7).toDate());
                query.setFilter(lastupdateFilter);
                Iterable<Entity> entities = ds.prepare(query).asIterable();
                List<Key> deleteKeys = new LinkedList<>();
                for (Entity e : entities) {
                    deleteKeys.add(e.getKey());
                    out.print(e.getKey());
                    out.println("<br>");
                }
                ds.delete(deleteKeys);
            } catch (Exception e) {
                out.println("<pre>" + e.getMessage() + "</pre>");
            }
            out.println("Query 2: Cup2Inloggegevens<br/>");
            try {
                Query query = new Query("Cup2Inloggegevens");
                Query.Filter lastupdateFilter = new Query.FilterPredicate("sessieLaatsteUpdate", Query.FilterOperator.LESS_THAN, DateTime.now().minusWeeks(7).toDate());
                Query.Filter inlogOKFilter = new Query.FilterPredicate("inlogOK", Query.FilterOperator.EQUAL, false);
                query.setFilter(Query.CompositeFilterOperator.and(lastupdateFilter, inlogOKFilter));
                Iterable<Entity> entities = ds.prepare(query).asIterable();
                List<Key> deleteKeys = new LinkedList<>();
                for (Entity e : entities) {
                    deleteKeys.add(e.getKey());
                    out.print(e.getKey());
                    out.println("<br>");
                }
                ds.delete(deleteKeys);
            } catch (Exception e) {
                out.println("<pre>" + e.getMessage() + "</pre>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
