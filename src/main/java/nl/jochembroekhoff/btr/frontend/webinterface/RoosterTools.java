/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Text;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup2.container.HistorieKeuze;
import nl.jochembroekhoff.btr.api.cup2.container.Les;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.PrintbaarRoosterResultaat;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.BasisLes;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.ToetsenLes;
import nl.jochembroekhoff.btr.api.rooster.deel.LaadRooster;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;
import nl.jochembroekhoff.btr.api.roostering.LesTijd;
import nl.jochembroekhoff.btr.api.toetsrooster.ApiToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.LaadToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.LaadToetsroosterResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.Constanten;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.IndexToetsrooster;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Weeks;
import org.jsoup.nodes.Element;

/**
 *
 * @author Jochem
 */
public class RoosterTools {

    public static LaadRoosterResultaat laadRooster(String code, String type, Week week, String locatie) {
        LaadRoosterResultaat lrr = LaadRooster.laadRooster(code, type, week, locatie);
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        String dsid = code.toLowerCase() + "|" + type + "|" + week.getIdentificator() + "|" + locatie;

        if (!lrr.gelukt()) {
            //Proberen op te halen uit datastore
            Entity lesrooster = new Entity("Lesrooster", dsid);

            try {
                lesrooster = ds.get(lesrooster.getKey());
                Object binser = lesrooster.getProperty("binser_compressed");
                if (binser != null && Text.class.isAssignableFrom(binser.getClass())) {
                    lrr = BtrApi.stringToSerializable(((Text) binser).getValue(), LaadRoosterResultaat.class, true);
                    System.out.println("EntityRestoreSucccess: " + dsid);
                } else {
                    System.out.println("EntityNullOrNoText: " + dsid);
                }
            } catch (EntityNotFoundException enfe) { //Entiteit niet gevonden in datastore
                //TODO: Fout verwerken?
                System.out.println("EntityNotFound: " + dsid);
            } catch (IOException | ClassNotFoundException e) { //Fout tijdens het uitlezen van het LRR
                //TODO
                System.out.println("FailedDeser: " + dsid);
            }
        } else {
            try {
                //Opslaan in datastore
                Entity lesrooster = new Entity("Lesrooster", dsid);
                String opTeSlaan = BtrApi.serializableToString(lrr, true);
                lesrooster.setIndexedProperty("lastupdate", DateTime.now().toDate());
                lesrooster.setProperty("binser_compressed", new Text(opTeSlaan));
                ds.put(lesrooster);
                System.out.println("EntitySaved: " + dsid);
            } catch (IOException e) {
                //Fout verwerken?
                System.out.println("FailedSer: " + dsid);
            }
        }

        return lrr; //TODO: Nullcheck
    }

    public static String renderLaadRoosterResultaat(LaadRoosterResultaat lrr, PrintbaarRoosterResultaat prr, String code, String locatie, boolean laadToetsen, boolean geladenOpWeergeven) {
        StringBuilder render = new StringBuilder();

        boolean renderCup = prr != null && prr.gelukt();
        boolean renderCupMislukt = prr != null && !prr.gelukt();

        if (laadToetsen) {
            IndexToetsrooster.indexToetsrooster();
        }
        Map<Toetsweek, LaadToetsroosterResultaat> ltrs = new HashMap<>();

        if (!lrr.gelukt()) {
            render.append("<i>Geen rooster gevonden</i>");
        } else {

            String dagen = "";
            int iDag = 0;
            for (String dag : Constanten.DAGEN_WEERGAVE) {
                DateTime dtDag = lrr.getWeek().getDatum().plusDays(iDag);
                dagen += "<th style=\"text-align:center;\">" + dag + " (" + dtDag.getDayOfMonth() + "-" + dtDag.getMonthOfYear() + ")</th>\n";
                iDag++;
            }

            final Map<Integer, Element> lesTrs = new HashMap<>(); //Tabelrijen
            final Map<Integer, Boolean> welOfNietWeergeven = new HashMap<>();
            render.append("<table style=\"white-space:nowrap;\">\n<tr><th><!--LesUur--></th>");
            render.append(dagen);
            render.append("</tr>");
            for (BasisLes[] dag : lrr.getRooster2d()) {
                if (dag != null) {
                    int iLes = 0;
                    for (BasisLes les : dag) {
                        if (!lesTrs.containsKey(iLes)) {
                            Element nieuweTr = new Element("tr")
                                    .attr("style", "height:30px");
                            String lestijd = iLes + "e";
                            LesTijd tijden = lrr.getLestijden()[iLes];

                            if (tijden != null) {
                                lestijd += " (";
                                lestijd += tijden.getStart().getUur() + ":" + tijden.getStart().getMinuut();
                                lestijd += "-" + tijden.getEinde().getUur() + ":" + tijden.getEinde().getMinuut() + ")";
                            }

                            nieuweTr.appendChild(new Element("td").attr("style", "text-align:right;font-size:smaller;padding-right:5px;").text(lestijd));
                            lesTrs.put(iLes, nieuweTr);

                            if (!welOfNietWeergeven.getOrDefault(iLes, false)) {
                                welOfNietWeergeven.put(iLes, false);
                            }
                        }
                        if (les != null) {
                            for (int i = iLes; i < iLes + les.getLesSpan(); i++) {
                                welOfNietWeergeven.put(i, true);
                            }

                            Element tr = lesTrs.get(les.getLesUur());
                            Element td = new Element("td").attr("style", "border:1px solid grey;padding:5px;text-align:center;").attr("rowspan", les.getLesSpan() + "");
                            DateTime lesDatum = lrr.getWeek().getDatum().withDayOfWeek(les.getDag());

                            boolean zUurGerenderd = false;
                            boolean toetsenGerenderd = false;
                            if (renderCup
                                    && "ZUurLes".equals(les.getLesType())) {

                                for (HistorieKeuze keuze : prr.getKeuzes()) {
                                    if (keuze.getLesuur() == les.getLesUur()
                                            && keuze.getDatum().getYear() == lesDatum.getYear()
                                            && keuze.getDatum().getMonthOfYear() == lesDatum.getMonthOfYear()
                                            && keuze.getDatum().getDayOfMonth() == lesDatum.getDayOfMonth()) {
                                        zUurGerenderd = true;
                                        Les cupLes = keuze.getLes();

                                        //TODO: Dit is nogal rommelig --> betere rendering
                                        Element infoDiv = new Element("div");
                                        infoDiv.appendChild(new Element("span").attr("style", "color:blue;").text(cupLes.getVak()));
                                        infoDiv.appendChild(new Element("span").attr("style", "color:blue;").text(cupLes.getDocent()));
                                        infoDiv.appendChild(new Element("span").attr("style", "color:blue;").text(cupLes.getLokaal()));
                                        if (!StringUtils.isBlank(cupLes.getExtra())) {
                                            infoDiv.appendChild(new Element("span").attr("style", "color:blue;").text("(" + cupLes.getExtra() + ")"));
                                        }
                                        td.appendChild(infoDiv);
                                    }
                                }
                            }
                            if (!zUurGerenderd && laadToetsen
                                    && "ToetsenLes".equals(les.getLesType())) {

                                //Laad het toetsrooster
                                LaadToetsroosterResultaat ltr = laadToetsroosterResultaat((ToetsenLes) les, ltrs, code, locatie);

                                if (ltr != null && ltr.gelukt()) {
                                    //doe rendering stuff
                                    System.out.println("TW gevonden: " + ltr.getToetsweek().getBeschrijving());

                                    toetsenGerenderd = true;
                                }
                            }
                            if (!zUurGerenderd && !toetsenGerenderd) {
                                les.getRoosterTekst().forEach((rt) -> {
                                    Element span = new Element("span");
                                    rt.getVelden().forEach((veld) -> {
                                        Element innerSpan = new Element("span").text(veld.getTekst());
                                        String style = "margin:5px;";
                                        if (veld.isDoorgestreept()) {
                                            style += "text-decoration:line-through;";
                                        }
                                        if (veld.isTussentekst()) {
                                            style += "font-style:italic;";
                                        }
                                        if (veld.isWijziging()) {
                                            style += "color:red;";
                                        }
                                        span.appendChild(innerSpan.attr("style", style));
                                    });
                                    td.appendChild(span);
                                    td.appendElement("br");
                                });
                            }
                            tr.appendChild(td);
                        }

                        iLes++;
                    }
                }
            }
            lesTrs.forEach((lesIndex, tr) -> {
                try {
                    if (welOfNietWeergeven.getOrDefault(lesIndex, Boolean.FALSE)) {
                        render.append(tr);
                    }
                } catch (NullPointerException npe) {
                    //ok... niet weergeven ofzo
                }
            });
            render.append("</table>");

            String dtGeladen = DateTime.now(DateTimeZone.forID("Europe/Amsterdam")).toString(BtrApi.DT_FORMATTER_MAAND_DAG_UUR_MINUUT);
            String dtLbo = lrr.getLaatstBijgewerktOp().toString(BtrApi.DT_FORMATTER_MAAND_DAG_UUR_MINUUT);

            if (geladenOpWeergeven) {
                render.append("<p><small>Geladen: ");
                render.append(dtGeladen);
            }
            render.append("<br>Bijgewerkt: ");
            render.append(dtLbo);
            render.append("</small></p>");
            if (renderCup) {
                render.append("<p><small>CUP-gegevens zijn niet leidend. "
                        + "Controleer op het roosterbord of je misschien niet "
                        + "ergens anders bent geplaatst.</small></p>");
            } else if (renderCupMislukt) {
                render.append("<p><small>Er is een poging gedaan om je CUP-uren "
                        + "in te laden in je rooster. Dit is helaas mislukt. "
                        + "<br/>Probeer opnieuw je gegevens in te voeren.</small></p>");
            }
        }

        return render.toString();
    }

    public static Map<Week, String> getWeekbeschrijvingen(List<Week> weken) {
        DateTime dezeWeek = DateTime.now(DateTimeZone.forID("Europe/Amsterdam"))
                .withDayOfWeek(1)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        Map<Week, String> weekBeschrijvingen = new HashMap<>();
        weken.forEach(indexWeek -> {
            String weergaveTekst;
            int weekVerschil = Weeks.weeksBetween(dezeWeek, indexWeek.getDatum()).getWeeks();
            switch (weekVerschil) {
                case -1:
                    weergaveTekst = "Vorige week";
                    break;
                case 0:
                    weergaveTekst = "Deze week";
                    break;
                case 1:
                    weergaveTekst = "Volgende week";
                    break;
                default:
                    weergaveTekst = Math.abs(weekVerschil) + " weken";
                    if (weekVerschil < 0) {
                        weergaveTekst += " terug";
                    } else {
                        weergaveTekst = "Over " + weergaveTekst;
                    }
                    break;
            }
            weekBeschrijvingen.put(indexWeek, weergaveTekst);
        });
        return weekBeschrijvingen;
    }

    public static LaadToetsroosterResultaat laadToetsroosterResultaat(ToetsenLes toetsenLes, Map<Toetsweek, LaadToetsroosterResultaat> ltrs, String code, String locatie) {
        //Bepaal welke toetsweek het is
        if (!ApiToetsrooster.INDEXERINGEN.containsKey(locatie)) {
            return null;
        }

        Toetsweek gebruikToetsweek = null;
//        int laatsteAfstand = Integer.MAX_VALUE;
//
//        System.out.println("=====START=====");
//        for (Toetsweek tw : ApiToetsrooster.INDEXERINGEN.get(locatie).getToetsweken()) {
//            String huidig = "";
//            for (RoosterTekst rt : toetsenLes.getRoosterTekst()) {
//                huidig += " " + rt.toString();
//            }
//            huidig = huidig.trim();
//
//            String vergelijk = tw.getBeschrijving();
//            int levAfstand = StringUtils.getLevenshteinDistance(huidig, vergelijk);
//
//            if (levAfstand < laatsteAfstand) {
//                laatsteAfstand = levAfstand;
//                gebruikToetsweek = tw;
//            }
//
//            System.out.println("-----");
//            System.out.println("HUIDIG\t\t" + huidig);
//            System.out.println("VRGL.M.\t" + vergelijk);
//            System.out.println("LEV.AFST.\t" + levAfstand);
//        }
//        System.out.println("WINN.AFST.\t\t" + laatsteAfstand);
//        System.out.println("WINN..\t\t" + gebruikToetsweek.getBeschrijving());
//        System.out.println("=====EINDE=====");

        List<Toetsweek> tws = ApiToetsrooster.INDEXERINGEN.get(locatie).getToetsweken().stream()
                .filter(tw -> tw.getBeschrijving().contains(toetsenLes.getToetsweekNummer() + ""))
                .collect(Collectors.toList());

        if (tws.isEmpty()) {
            return null;
        }

        if (tws.size() == 1) {
            gebruikToetsweek = tws.get(0);
        } else {
            //TODO: uitzoeken welke TW van toepassing is (met inhalen en herkansen enzo)
        }

        //Haal nieuwe data op zo nodig
        if (!ltrs.containsKey(gebruikToetsweek)) {
            try {
                LaadToetsroosterResultaat ltr = LaadToetsrooster.laadToetsrooster(code, gebruikToetsweek.getPad(), "leerlingen", locatie);
                if (ltr.gelukt()) {
                    ltrs.put(gebruikToetsweek, ltr);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        return ltrs.getOrDefault(gebruikToetsweek, null);
    }
}
