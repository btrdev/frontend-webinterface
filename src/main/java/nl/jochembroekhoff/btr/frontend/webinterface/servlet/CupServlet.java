/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet;

import com.google.appengine.api.datastore.*;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.frontend.webinterface.CupTools;
import nl.jochembroekhoff.btr.frontend.webinterface.SessieTools;
import nl.jochembroekhoff.btr.frontend.webinterface.security.Encryptor;
import org.javatuples.Pair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Jochem
 */
@WebServlet(name = "CupServlet", urlPatterns = {"/CupServlet"})
public class CupServlet extends HttpServlet {

    private static DatastoreService DS = DatastoreServiceFactory.getDatastoreService();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (!SessieTools.isIngelogd(request.getSession())) {
                out.println("{\"gelukt\":false,\"error\":\"NIET_INGELOGD\"}");
                return;
            }

            HttpSession sessie = request.getSession();
            String dataString = "{}";

            switch (request.getParameter("actie")) {
                case "getAuthStatus": {
                    Query query = new Query("CupLinkCache");
                    Query.Filter codeFilter = new Query.FilterPredicate("code", Query.FilterOperator.EQUAL, sessie.getAttribute("rooster_code") + "");
                    query.setFilter(codeFilter);

                    List<Entity> entities = DS.prepare(query).asList(FetchOptions.Builder.withLimit(1));

                    Entity linkEntity = entities.size() > 0
                            ? entities.get(0)
                            : null;

                    boolean gekoppeld = linkEntity != null;
                    boolean actiefInSessie = false;

                    Object ruwSessieData = sessie.getAttribute("cup_sessieData");
                    if (ruwSessieData != null) {
                        try {
                            SessieData sd = BtrApi.stringToSerializable(ruwSessieData + "", SessieData.class, true);
                            actiefInSessie = true;
                        } catch (Exception e) {
                            sessie.setAttribute("cup_sessieData", null);
                        }
                    }

                    dataString = "{\"gekoppeld\": " + gekoppeld + ", \"actief_in_sessie\": " + actiefInSessie + "}";

                    break;
                }
                case "setAuthStatusEersteKeer": {
                    String clientKey = request.getHeader("Client-Key");
                    String bewaartoken = request.getHeader("Bewaartoken");
                    Pair<Entity, SessieData> sessiee;
                    try {
                        sessiee = CupTools.getSessie(clientKey, bewaartoken);
                    } catch (Exception ex) {
                        sessiee = null;
                    }
                    if (sessie != null) {
                        Entity entity = sessiee.getValue0();
                        String iv = (String) entity.getProperty("iv");
                        String gebruikersnaam = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("gebruikersnaam")));

                        boolean nuGekoppeld = false;
                        //TODO: Check of de gebruiker wel wil koppelen
                        if (gebruikersnaam.contains(sessie.getAttribute("rooster_code") + "")) {
                            nuGekoppeld = true;

                            Query query = new Query("CupLinkCache");
                            Query.Filter codeFilter = new Query.FilterPredicate("code", Query.FilterOperator.EQUAL, sessie.getAttribute("rooster_code") + "");
                            query.setFilter(codeFilter);

                            List<Entity> entities = DS.prepare(query).asList(FetchOptions.Builder.withLimit(1));

                            Entity linkEntity;
                            if (entities.size() == 0) {
                                linkEntity = new Entity("CupLinkCache");
                                linkEntity.setProperty("code", sessie.getAttribute("rooster_code") + "");
                            } else {
                                linkEntity = entities.get(0);
                            }

                            linkEntity.setProperty("gebruikersnaam", sessie.getAttribute("rooster_code") + "");
                            linkEntity.setProperty("zoekeletters", ???); //TODO: Zoekletters

                            DS.put(linkEntity);
                        }

                        sessie.setAttribute("cup_sessieData", BtrApi.serializableToString(sessiee.getValue1(), true));

                        dataString = "{\"geactiveerd\": true, \"gekoppeld\": \"" + nuGekoppeld + "\"}";
                    }
                    break;
                }
                case "setAuthStatus": {
                    //TODO: Nieuwe sessie enzo met bewaartoken, dan zoeken ook enzo
                    break;
                }
                case "getKeuzeOpties":
                    //TODO
                    break;
                case "doIntekenen":
                    //TODO
                    break;
                default:
                    out.println("{\"gelukt\":false,\"error\":\"ACTIE_ONBEKEND\"}");
                    return;
            }

            out.println("{\"gelukt\":true,\"data\":" + dataString + "}");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
