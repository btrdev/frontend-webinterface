/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.LogIn;
import nl.jochembroekhoff.btr.api.cup2.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ZoekNamenResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.security.Encryptor;
import org.javatuples.Pair;

/**
 *
 * @author jochem
 */
public class CupTools {

    private static final DatastoreService DS = DatastoreServiceFactory.getDatastoreService();

    public static Pair<Entity, SessieData> getSessie(String clientKey, String bewaartoken) throws Exception {
        Key key = KeyFactory.stringToKey(bewaartoken);
        Entity entity = DS.get(key);

        SessieData sessieData = Encryptor.decrypt(
                clientKey,
                (String) entity.getProperty("iv"),
                SessieData.class,
                ((Text) entity.getProperty("sessieData")).getValue());

        return Pair.with(entity, sessieData);
    }

    public static boolean checkVereistOpnieuwInloggen(Entity entity) {
        Date sessieLaatsteUpdate = (Date) entity.getProperty("sessieLaatsteUpdate");
        long verschil = new Date().getTime() - sessieLaatsteUpdate.getTime();
        long verschilMinuten = TimeUnit.MILLISECONDS.toMinutes(verschil);
        return verschilMinuten > 3;
    }

    public static SessieData opnieuwInloggen(Entity entity, String clientKey) throws IllegalBlockSizeException, BadPaddingException {
        SessieData sessieData = ApiCup2.getNieuweSessie();
        ZoekNamenResultaat znr = ZoekNamen.zoekNamen(sessieData, (String) entity.getProperty("zoekletters"));
        if (!znr.gelukt()) {
            return null;
        }
        
        String iv = (String) entity.getProperty("iv");
        String gebruikersnaam = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("gebruikersnaam")));
        String pincode = new String(Encryptor.decrypt(clientKey, iv, (String) entity.getProperty("pincode")));

        LogInResultaat lir = LogIn.inloggen(znr.getSessieData(), gebruikersnaam, pincode);
        if (!lir.gelukt()) {
            return null;
        }
        
        return lir.getSessieData();
    }

    public static void updateSessie(Entity entity, String huidigeActie, String clientKey, SessieData sessieData) {
        entity.setProperty("sessieData", new Text(Encryptor.encrypt(
                clientKey,
                (String) entity.getProperty("iv"),
                sessieData)));
        entity.setProperty("sessieLaatsteUpdate", new Date());
        entity.setProperty("laatsteActie", huidigeActie);
        DS.put(entity);
    }
}
