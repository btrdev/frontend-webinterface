/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface;

import java.util.Calendar;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toets;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.LaadToetsroosterResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.Constanten;

/**
 *
 * @author Jochem
 */
public class ToetsroosterTools {

    public static String renderLaadToetsroosterResultaat(LaadToetsroosterResultaat lrr) {
        StringBuilder render = new StringBuilder();

        if (!lrr.gelukt()) {
            render.append("<i>Geen toetsrooster gevonden</i>");
        } else {
            //TODO: Betere weergave, datums enzo
            Calendar laatsteDatumKop = null;
            for (Toets toets : lrr.getToetsen()) {

                if (laatsteDatumKop == null || toets.getDatum().after(laatsteDatumKop)) {
                    laatsteDatumKop = toets.getDatum();
                    render.append("<hr/>");
                    render.append(Constanten.DAGEN_ALGEMEEN[laatsteDatumKop.get(Calendar.DAY_OF_WEEK) - 1]);
                    render.append(" ");
                    render.append(laatsteDatumKop.get(Calendar.DAY_OF_MONTH));
                    render.append(" ");
                    render.append(Constanten.MAANDAN[laatsteDatumKop.get(Calendar.MONTH)]);
                    render.append(" ");
                    render.append(laatsteDatumKop.get(Calendar.YEAR));
                    render.append("<br/>");
                }

                render.append("<b>");
                render.append(toets.getTijd().toString());
                render.append(" uur</b> ");
                render.append("<i>");
                render.append(toets.getVak());
                render.append("</i> ");
                render.append("<span>");
                render.append(toets.getOnderdeel());
                render.append("</span> ");
                render.append("<i>(");
                render.append(toets.getRuimte());
                render.append(", ");
                render.append(toets.getWerkelijkeDuur().toMinuten());
                render.append(" min.)</i> ");
                render.append("<br>\n");
            }

            if (lrr.getToetsen().length == 0) {
                if (lrr.getToetsweek().getType() == Toetsweek.ToetsweekType.CENTRAAL_EXAMEN_INHALEN
                        || lrr.getToetsweek().getType() == Toetsweek.ToetsweekType.SCHOOLEXAMEN_INHALEN) {
                    render.append("<i>Op deze inhaaldag(en)/herkansingendag(en) heb je waarschijnlijk geen toetsen</i>");
                } else {
                    render.append("<i>In deze toetsweek heb je waarschijnlijk geen toetsen</i>");
                }
            }

            if (lrr.getExamennummer() > -1) {
                render.append("<br>\n Examennummer: ");
                render.append(lrr.getExamennummer());
            }
        }

        return render.toString();
    }
}
