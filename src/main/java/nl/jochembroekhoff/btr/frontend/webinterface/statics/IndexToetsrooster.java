/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.statics;

import nl.jochembroekhoff.btr.api.toetsrooster.ApiToetsrooster;

/**
 *
 * @author Jochem Broekhoff
 */
public class IndexToetsrooster {

    private static boolean GEINDEXEERD = false;
    private static boolean RESET = false;

    /**
     * Reset de indexstatus zodat de volgen
     */
    public static void resetIndex() {
        RESET = true;
    }

    public static void indexToetsrooster() {
        if (!GEINDEXEERD || RESET) {
            GEINDEXEERD = true;
            RESET = false;
            ApiToetsrooster.setToetsroosterUrl("http://rooster.calvijncollege.nl/Toetsrooster/");
            ApiToetsrooster.LOCATIES.put("Goes", "Goes Klein Frankrijk");
            ApiToetsrooster.LOCATIES.put("GoesStationspark", "Goes Stationspark");
            ApiToetsrooster.LOCATIES.put("Middelburg", "Middelburg");
            ApiToetsrooster.LOCATIES.put("Tholen", "Tholen");
            RESET = !ApiToetsrooster.indexeer();
        }
    }
}
