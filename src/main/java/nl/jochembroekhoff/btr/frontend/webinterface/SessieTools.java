/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface;

import javax.servlet.http.HttpSession;

/**
 *
 * @author Jochem
 */
public class SessieTools {

    public static boolean isIngelogd(HttpSession sessie) {
        Object ingelogd_ = sessie.getAttribute("ingelogd");
        if (ingelogd_ != null) {
            return (boolean) ingelogd_;
        }
        return false;
    }
}
