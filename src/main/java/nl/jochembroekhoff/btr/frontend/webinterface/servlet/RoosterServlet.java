/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import nl.jochembroekhoff.btr.api.rooster.container.Week;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.RoosterTools;
import nl.jochembroekhoff.btr.frontend.webinterface.SessieTools;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.IndexRooster;
import org.jsoup.nodes.Element;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "RoosterServlet", urlPatterns = {"/RoosterServlet"})
public class RoosterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (request.getParameter("weekId") != null) {
                request.getSession().setAttribute("rooster_weekid", request.getParameter("weekId"));
                response.sendRedirect("./overzicht.jsp#!/rooster");
            } else if (SessieTools.isIngelogd(request.getSession())) {
                String outputType = request.getParameter("outputType");

                if (outputType == null || outputType.length() == 0) {
                    outputType = "json";
                }

                //Maak een index van het rooster indien nodig
                IndexRooster.indexRooster();

                LocalDate date = LocalDate.now();
                WeekFields weekFields = WeekFields.of(Locale.getDefault());
                int weekNumber = date.get(weekFields.weekOfWeekBasedYear());

                String roosterWeekId = request.getSession().getAttribute("rooster_weekid") + "";
                //TODO: Check of de locatie wel bestaat.
                List<Week> weken = ApiRooster.INDEXERINGEN.get(request.getSession().getAttribute("rooster_locatie") + "").getWeken();

                if (roosterWeekId == null || roosterWeekId.equals("null") || roosterWeekId.length() == 0) {
                    Optional<Week> week = weken.stream().filter(w -> w.getWeeknummer() == weekNumber).findFirst();
                    if (week.isPresent()) {
                        roosterWeekId = week.get().getIdentificator();
                    } else {
                        roosterWeekId = weken.get(0).getIdentificator();
                    }
                }

                Week laadWeek = null;
                for (Week week : weken) {
                    if (week.getIdentificator().equals(roosterWeekId)) {
                        laadWeek = week;
                    }
                }

                Map<Week, String> weekBeschrijvingen = RoosterTools.getWeekbeschrijvingen(weken);

                String code = request.getSession().getAttribute("rooster_code") + "";
                String locatie = request.getSession().getAttribute("rooster_locatie") + "";
                LaadRoosterResultaat lrr = RoosterTools.laadRooster(code, request.getSession().getAttribute("rooster_type") + "", laadWeek, locatie);
                Gson gson = new GsonBuilder().setPrettyPrinting().create();

                switch (outputType) {
                    case "html":
                        response.setContentType("text/html;charset=UTF-8");

                        String dezeWeek = weekBeschrijvingen.get(laadWeek) + " (week " + laadWeek.getWeeknummer() + ")";
                        out.println("<p><b>" + dezeWeek + "</b></p>");
                        out.println("<form method=POST action=./RoosterServlet id=RoosterSelectWeekId><select name=weekId onchange='$(\"#RoosterSelectWeekId\").submit();' ><option value='' style='display:none' disabled selected>Andere week...</option>");
                        //Keuzeopties voor weken
                        weken.forEach(week -> {
                            out.println(new Element("option")
                                    .val(week.getIdentificator())
                                    .text(weekBeschrijvingen.get(week)));
                        });
                        out.println("</select></form>");
                        out.println(RoosterTools.renderLaadRoosterResultaat(lrr, null, code, locatie, true, false));
                        break;
                    case "json":
                    default:
                        out.println(gson.toJson(lrr));
                        break;
                }
            } else {
                out.println("NIET INGELOGD");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
