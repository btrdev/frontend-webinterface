/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.statics;

import nl.jochembroekhoff.btr.api.rooster.ApiRooster;

/**
 *
 * @author Jochem
 */
public class IndexRooster {

    private static boolean GEINDEXEERD = false;
    private static boolean RESET = false;
    
    /**
     * Reset de indexstatus zodat de volgen
     */
    public static void resetIndex() {
        RESET = true;
    }

    public static void indexRooster() {
        if (!GEINDEXEERD || RESET) {
            System.out.println("Nieuwe RoosterIndex maken...");
            GEINDEXEERD = true;
            RESET = false;
            ApiRooster.setTimeout(2000);
            ApiRooster.setRoosterUrl("http://rooster.calvijncollege.nl/");
            ApiRooster.LOCATIES.put("Goes", "Goes Klein Frankrijk");
            ApiRooster.LOCATIES.put("GoesNoordhoeklaan", "Goes Noordhoeklaan");
            ApiRooster.LOCATIES.put("GoesStationspark", "Goes Stationspark");
            ApiRooster.LOCATIES.put("KrabbendijkeAppelstraat", "Krabbendijke Appelstraat");
            ApiRooster.LOCATIES.put("KrabbendijkeKerkpolder", "Krabbendijke Kerkpolder");
            ApiRooster.LOCATIES.put("Middelburg", "Middelburg");
            ApiRooster.LOCATIES.put("Tholen", "Tholen");
            RESET = !ApiRooster.indexeer(); //Als het misgaat, de index volgende keer nog een keer proberen te maken
        }
    }
}
