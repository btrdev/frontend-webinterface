/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.jochembroekhoff.btr.api.toetsrooster.ApiToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Indexering;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.LaadToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.LaadToetsroosterResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.ToetsroosterTools;
import nl.jochembroekhoff.btr.frontend.webinterface.SessieTools;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.IndexToetsrooster;
import org.jsoup.nodes.Element;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "ToetsroosterServlet", urlPatterns = {"/ToetsroosterServlet"})
public class ToetsroosterServlet extends HttpServlet {

    //TODO: Move to a collection class
    public static final List<String> TOETSROOSTER_LOCATIES = Arrays.asList("Goes", "GoesStationspark", "Middelburg", "Tholen");

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (request.getParameter("selecteerToetsweek") != null) {
                request.getSession().setAttribute("geselecteerde_toetsweek", request.getParameter("selecteerToetsweek"));
                response.sendRedirect("./overzicht.jsp#!/toetsrooster");
            } else {

                if (SessieTools.isIngelogd(request.getSession())) {
                    String outputType = request.getParameter("outputType");

                    if (outputType == null || outputType.length() == 0) {
                        outputType = "html";
                    }

                    //Maak een index van het rooster indien nodig
                    IndexToetsrooster.indexToetsrooster();

                    String code = request.getSession().getAttribute("rooster_code") + "";
                    String type = request.getSession().getAttribute("rooster_type") + "";
                    String locatie = request.getSession().getAttribute("rooster_locatie") + "";
                    Object tw = request.getSession().getAttribute("geselecteerde_toetsweek");
                    if (type.equals("s") && TOETSROOSTER_LOCATIES.contains(locatie)) {
                        Indexering index = ApiToetsrooster.INDEXERINGEN.get(locatie);

                        if (index.getToetsweken().isEmpty()) {
                            printGeenToetsroosterGevonden(response, out);
                        } else {
                            String toetsweekPad;
                            if (tw == null) {
                                //Gebruik eerste toetsweek dan maar
                                toetsweekPad = index.getToetsweken().get(index.getToetsweken().size() - 1).getPad();
                            } else {
                                toetsweekPad = tw + "";
                            }

                            LaadToetsroosterResultaat ltr = LaadToetsrooster.laadToetsrooster(code, toetsweekPad, "leerlingen", locatie);
                            Gson gson = new GsonBuilder().setPrettyPrinting().create();

                            switch (outputType) {
                                case "json":
                                    out.println(gson.toJson(ltr));
                                    break;
                                case "html":
                                default:
                                    Toetsweek geladenTw = ltr.getToetsweek();
                                    response.setContentType("text/html;charset=UTF-8");
                                    out.println("<p><b>Toetsweek: " + geladenTw.getBeschrijving() + "</b></p>");
                                    out.println("<form method=POST action=./ToetsroosterServlet id=ToetsroosterSelectToetsweek><select name=selecteerToetsweek onchange='$(\"#ToetsroosterSelectToetsweek\").submit();' ><option value='' style='display:none' disabled selected>Andere toetsweek...</option>");
                                    ApiToetsrooster.INDEXERINGEN.get(ltr.getLocatieSlug()).getToetsweken().forEach((eenToetsweek) -> {
                                        out.println(new Element("option")
                                                .val(eenToetsweek.getPad())
                                                .text(eenToetsweek.getBeschrijving()));
                                    });
                                    out.println("</select></form>");
                                    out.println(ToetsroosterTools.renderLaadToetsroosterResultaat(ltr));
                                    break;
                            }
                        }
                    } else {
                        switch (outputType) {
                            case "html":
                            default:
                                printGeenToetsroosterGevonden(response, out);
                                break;
                        }
                    }
                } else {
                    out.println("NIET INGELOGD");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void printGeenToetsroosterGevonden(HttpServletResponse response, PrintWriter out) {
        response.setContentType("text/html;charset=UTF-8");
        out.println("<i>Geen toetsrooster gevonden</i>");
    }

}
