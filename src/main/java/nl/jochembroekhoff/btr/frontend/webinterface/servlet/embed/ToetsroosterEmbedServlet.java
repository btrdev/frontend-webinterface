/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.servlet.embed;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.jochembroekhoff.btr.api.toetsrooster.ApiToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Indexering;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.LaadToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.LaadToetsroosterResultaat;
import nl.jochembroekhoff.btr.frontend.webinterface.ToetsroosterTools;
import nl.jochembroekhoff.btr.frontend.webinterface.servlet.ToetsroosterServlet;
import nl.jochembroekhoff.btr.frontend.webinterface.statics.IndexToetsrooster;

/**
 *
 * @author Jochem
 */
@WebServlet(name = "ToetsroosterEmbedServlet", urlPatterns = {"/embed/ToetsroosterEmbedServlet"})
public class ToetsroosterEmbedServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //TODO: Index reset calls
            //Maak een index van het rooster indien nodig
            IndexToetsrooster.indexToetsrooster();

            String code = request.getParameter("code");
            String toetsweekPad = request.getParameter("toetsweek");
            String type = request.getParameter("type");
            String locatie = request.getParameter("locatie");
            String indexOphalen = request.getParameter("indexOphalen");

            if (indexOphalen != null && locatie != null) {
                //
                // Index ophalen
                //
                ApiToetsrooster.indexeer();
                Indexering index = ApiToetsrooster.INDEXERINGEN.get(locatie);
                response.setContentType("text/plain;charset=UTF-8");
                if (index == null) {
                    out.println("ERR|Index niet gevonden voor locatie " + locatie);
                } else {
                    index.getToetsweken().forEach(tw -> {
                        out.println(tw.getPad() + "|" + tw.getBeschrijving());
                    });
                }
            } else if (code != null && toetsweekPad != null && type != null && locatie != null
                    && ToetsroosterServlet.TOETSROOSTER_LOCATIES.contains(locatie)) {
                //
                // Toetsrooster ophalen
                //
                Indexering index = ApiToetsrooster.INDEXERINGEN.get(locatie);

                if (index.getToetsweken().isEmpty()) {
                    ToetsroosterServlet.printGeenToetsroosterGevonden(response, out);
                } else {
                    LaadToetsroosterResultaat ltr = LaadToetsrooster.laadToetsrooster(code, toetsweekPad, type, locatie);
                    out.println(ToetsroosterTools.renderLaadToetsroosterResultaat(ltr));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
