/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.frontend.webinterface.statics;

import java.io.PrintWriter;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import org.apache.commons.lang3.mutable.MutableBoolean;

/**
 *
 * @author jochem
 */
public enum AlgemeneParameter {
    LOCATIE("locatie"),
    TYPE("type");

    private static final String[] TYPES = new String[]{"c", "t", "r", "s"};
    private final String naam;

    AlgemeneParameter(String naam) {
        this.naam = naam;
    }

    public static String checkParameterWaarde(HttpServletRequest req, PrintWriter out, AlgemeneParameter parameter, MutableBoolean check) {
        String waarde = req.getParameter(parameter.naam);
        if (waarde == null) {
            check.setValue(false);
            out.println("ERR|PARAMETER_ONTBREEKT|" + parameter.naam);
            return null; //TODO: Als vereist --> foutmelding
        }

        switch (parameter) {
            case LOCATIE:
                if (!ApiRooster.LOCATIES.containsKey(waarde)) {
                    check.setValue(false);
                    out.println("ERR|LOCATIE_ONBEKEND");
                }
                break;
            case TYPE:
                if (!Arrays.asList(TYPES).contains(waarde)) {
                    check.setValue(false);
                    out.println("ERR|TYPE_ONBEKEND");
                }
            default:
                break;
        }

        return waarde;
    }
}
