/*!
 * (c) 2017  Jochem Broekhoff
 * Alle rechten voorbehouden - All rights reserved
 * 
 * Pagina Overzicht Script
 */

if (typeof window.sessionStorage['clientKey'] !== 'string')
    window.sessionStorage['clientKey'] = BTR.ui.genId().replace(/-/g, "");

BTR.views.definitions = {
    _default: "#!/rooster",
    rooster: {
        _title: "Rooster",
        _load: function ($container) {
        },
        _reload: function ($container) {
            $.get("/RoosterServlet?outputType=html", function (data) {
                $container.html(data);
            });
        }
    },
    toetsrooster: {
        _title: "Toetsrooster",
        _load: function ($container) {
        },
        _reload: function ($container) {
            $.get("/ToetsroosterServlet?outputType=html", function (data) {
                $container.html(data);
            });
        }
    },
    cup: {
        _title: "CUP",
        actief: false,
        idTabs: BTR.ui.genId(),
        maakTabsLeeg: function () {
            $("#" + this.idTabs).empty();
        },
        _load: function ($container) {
            //Check linkstatus
            $.get("/CupServlet?actie=getAuthStatus", function (response) {
                if (response.gelukt === true) {
                    var data = response.data;
                    if (!data.gekoppeld) {
                        $.ajax("/api/CupApiServlet", {
                            method: "POST",
                            headers: {
                                "Client-Key": window.sessionStorage['clientKey']
                            },
                            success: function (resp) {
                                if (resp.startsWith("BEWAARTOKEN|")) {
                                    window.sessionStorage['bewaartoken'] = resp.substring(12).trim();

                                    var zoekletters = prompt("Vul de eerste 3 tot 7 letters van je achternaam in:");
                                    if (typeof zoekletters === 'string' && zoekletters.length > 0) {
                                        $.ajax("/api/CupApiServlet", {
                                            method: "POST",
                                            headers: {
                                                "Client-Key": window.sessionStorage['clientKey'],
                                                Bewaartoken: window.sessionStorage['bewaartoken']
                                            },
                                            data: {
                                                actie: "zoekNamen",
                                                zoekletters: zoekletters
                                            },
                                            success: function (resp) {
                                                var $select = $("<select></select>");

                                                resp.trim().split("\n").forEach(function (value, index) {
                                                    var naamEnWaarde = value.split("|");
                                                    $select.append($("<option></option>")
                                                        .text(naamEnWaarde[1])
                                                        .val(naamEnWaarde[0]));
                                                });

                                                $container.append($select);
                                                $container.append($("<button></button>")
                                                    .text("Verder")
                                                    .click(function () {
                                                        var pincode = prompt("Voer de pincode in");
                                                        if (typeof pincode === 'string' && pincode.length > 0) {
                                                            $.ajax("/api/CupApiServlet", {
                                                                method: "POST",
                                                                headers: {
                                                                    "Client-Key": window.sessionStorage['clientKey'],
                                                                    Bewaartoken: window.sessionStorage['bewaartoken']
                                                                },
                                                                data: {
                                                                    actie: "logIn",
                                                                    gebruikersnaam: $select.val(),
                                                                    pincode: pincode
                                                                },
                                                                success: function (respLogIn) {
                                                                    if (respLogIn.trim() === "OK") {
                                                                        $.ajax("/CupServlet", {
                                                                            method: "POST",
                                                                            headers: {
                                                                                "Client-Key": window.sessionStorage['clientKey'],
                                                                                Bewaartoken: window.sessionStorage['bewaartoken']
                                                                            },
                                                                            data: {
                                                                                actie: "setAuthStatusEersteKeer",
                                                                                koppel: confirm("Wil je deze CUP-login koppelen aan dit leerlingnummer? (Werkt alleen als het leerlingnummer uit CUP overeenkomt het het leerlingnummer van de actieve gebruiker.)")
                                                                            },
                                                                            success: function (respSetAuthStatus) {
                                                                                BTR.views.definitions.cup._reload($container);
                                                                            }
                                                                        })
                                                                    } else {
                                                                        alert("Fout: " + respLogIn.substring(4));
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }));
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    } else {
                        if (!data.actief_in_sessie) {
                            $.ajax("/api/CupApiServlet", {
                                method: "POST",
                                headers: {
                                    "Client-Key": window.sessionStorage['clientKey']
                                },
                                success: function (resp) {
                                    if (resp.startsWith("BEWAARTOKEN|")) {
                                        window.sessionStorage['bewaartoken'] = resp.substring(12).trim();
                                        BTR.views.definitions.cup.actief = true;
                                        BTR.views.definitions.cup._reload($container);

                                    }
                                }
                            });
                        } else {
                            BTR.views.definitions.cup.actief = true;
                            BTR.views.definitions.cup._reload($container);
                        }
                    }
                }
            });
        },
        _reload: function ($container) {
            if (this.actief) {
                $container.empty();
                $container.append($("<div></div>")
                    .addClass("tabs")
                    .append($("<ul></ul>")
                        .attr("id", this.idTabs)));
                //TODO: Laad contacturenoverzicht
            }
        }
    },
    info: {
        _title: "Info",
        _load: function ($container) {
            $container.html('<a href="./doc/api.html">API-documentatie</a><br/><a href="./doc/embed.html">Embed-documentatie</a>');
        }
    }
};

BTR.ui.views.activate = function (view) {
    $("#sidebar li a").each(function () {
        var $this = $(this);
        $this.removeClass("is-active");
        if ($this.attr("href") === "#!/" + view)
            $this.addClass("is-active");
    });
};
BTR.ui.views.provide = function (view) {
    return $("#ui-view-" + view);
};
BTR.ui.views.activated = function (view, controller, $view) {
    try {
        BTR.ui.updateTitel(controller._title);
    } catch (e) {
        BTR.log.w(e);
    } finally {
        //Crash reporten?
    }
};

$(document).ready(function () {
    for (var viewNaam in BTR.views.definitions) {
        if (viewNaam !== "_default") {
            var inhoud = document.getElementById("ui-inhoud");
            var nieuweDiv = document.createElement("div");
            nieuweDiv.classList += " container";
            nieuweDiv.id = "ui-view-" + viewNaam;
            nieuweDiv.style.display = "none";
            inhoud.appendChild(nieuweDiv);
        }
    }

    BTR.init();

    $("#sidebar li a").click(function (e) {
        BTR.views.activate($(this).attr("href"));
    });
});