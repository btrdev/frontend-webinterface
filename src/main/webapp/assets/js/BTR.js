/*!
 * (c) 2017  Jochem Broekhoff
 * Alle rechten voorbehouden - All rights reserved
 * 
 * BTR Library Script
 */

if (window.btrVerbose === undefined)
    window['btrVerbose'] = false;

var BTR = {
    check: {
        exception: function (input) {
            return input !== undefined
                    ? (input.name !== undefined && input.message !== undefined)
                    : false;
        }
    },
    log: {
        w: function (e) {
            if (BTR.check.exception(e)) {
                console.error("[BTR/Warning/Exception]: (" + e.name + "): " + e.message);
            } else {
                console.warn("[BTR/Warning]: " + e);
            }
        },
        i: function (e) {
            if (BTR.check.exception(e) && window.btrVerbose) {
                console.info("[BTR/Info/Exception]: (" + e.name + "): " + e.message);
            } else if (window.btrVerbose) {
                console.info("[BTR/Info]: " + e);
            }
        },
        v: function (e) {
            if (BTR.check.exception(e) && window.btrVerbose) {
                console.debug("[BTR/Verbose/Exception]: (" + e.name + "): " + e.message);
            } else if (window.btrVerbose) {
                console.debug("[BTR/Verbose]: " + e);
            }
        }
    },
    ui: {
        genId: function () {
            /**
             * @return {string}
             */
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
        },
        updateTitel: function (titel) {
            $("#ui-title").text(titel);
        },
        views: {
            /**
             * Activate a UI element to indicate the view is active.
             * Impl. is up to user.
             * @param {string} view
             * @returns {undefined}
             */
            activate: function (view) { },
            /**
             * Provide the jQuery element for a view.
             * Impl. is up to user.
             * @param {string} view
             * @returns jQuery
             */
            provide: function (view) {},
            /**
             * Invoked when a view has been loaded successfully.
             * Impl. is up to user.
             * @param {string} view
             * @param {type} controller
             * @param {jQuery} $view
             * @returns {undefined}
             */
            activated: function (view, controller, $view) { }
        }
    },
    views: {
        definitions: {},
        activateDefault: function () {
            BTR.log.v("Activating default view: " + this.definitions._default);
            this.activate(this.definitions._default);
        },
        activate: function (_view) {
            this.hideAll();

            var view = _view.replace("#!/", "");
            BTR.log.v("Activating view: " + view);

            if (!this.definitions.hasOwnProperty(view)) {
                BTR.log.w("View not found: " + view);
                BTR.log.v("Falling back to loading default view.");
                this.activateDefault();
                return;
            }

            BTR.ui.views.activate(view);

            var $view = BTR.ui.views.provide(view);
            $view.show();

            var controller = this.definitions[view];
            if (typeof controller._loaded === 'undefined') {
                //Invoke priming load
                controller._loaded = true;
                controller._load($view);
            }

            BTR.ui.views.activated(view, controller, $view);

            try {
                //Try to reload
                controller._reload($view);
            } catch (e) {
                BTR.log.v("This view probably has no _reload implementation: " + view);
                BTR.log.w(e);
            } finally {
                //Crash report?
            }
        },
        hideAll: function () {
            for (var view in this.definitions) {
                var $view = BTR.ui.views.provide(view);
                if (typeof $view !== 'undefined') {
                    $view.hide();
                }
            }
        },
        /**
         * Initialise the views functionality
         * @returns {undefined}
         */
        init: function () {
            var afterHash = window.location.hash;
            if (afterHash.length > 4) {
                this.activate(afterHash);
            } else if (typeof this.definitions._default !== 'undefined') {
                this.activateDefault();
            }
        }
    },
    /**
     * Initialise BTR-JS
     * @returns {undefined}
     */
    init: function () {
        try {
            this.views.init();
        } catch (e) {
            BTR.log.w(e);
        }
    }
};
