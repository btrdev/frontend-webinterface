/*!
 * (c) 2017  Jochem Broekhoff
 * Alle rechten voorbehouden - All rights reserved
 * 
 * Pagina WelkomPagina Script
 */
/*global $*/
/*global BTR*/
BTR.views.definitions = {
    _default: "#!/main",
    main: {
        _title: "Welkom",
        _load: function ($container) {
            /*
            $("#inputWachtwoord, #inputType").change(function () {
                var inpType = $("#inputType").val();
                var inpLocatie = $("#inputWachtwoord").val();
                var $prefix = $("#inputTypePrefix");
                if (inpType === "r" && inpLocatie === "Goes") {
                    $prefix.show();
                } else {
                    $prefix.hide();
                }
            });
            */
        },
        _reload: function ($container) {}
    }
};

BTR.ui.views.provide = function (view) {
    return $("#ui-view-" + view);
};
BTR.ui.views.activated = function (view, controller, $view) {
    try {
        BTR.ui.updateTitel(controller._title);
    } catch (e) {
        BTR.log.w(e);
    } finally {
        //Ooit nog iets, trace loggen?
    }
};

$(document).ready(function () {
    BTR.init();
});