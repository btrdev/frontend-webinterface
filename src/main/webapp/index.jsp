<%-- 
    Document   : index
    Created on : Sep 6, 2017, 1:06:08 PM
    Author     : Jochem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import = "java.io.*,java.util.*,nl.jochembroekhoff.btr.frontend.webinterface.HtmlTools,nl.jochembroekhoff.btr.frontend.webinterface.SessieTools" %>

<%
    if (SessieTools.isIngelogd(request.getSession())) {
        response.sendRedirect("./overzicht.jsp");
    }
%>

<!DOCTYPE html>
<html lang="nl">
    <head>
        <%= HtmlTools.getBasicHead("Inloggen") %>
    </head>
    <body>

        <section class="section">
            <div class="container">
                <h1 id="ui-title" class="title">Welkom</h1>
                <h2 class="subtitle">BTRooster</h2>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="columns" id="ui-view-main">
                    <div class="colum is-one-third-desktop">
                        <form method="POST" action="./AuthServlet">
                            <div class="field">
                                <label class="label">Code</label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="Code" name="gebruikersnaam" autofocus required/>
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Type</label>
                                <div class="control">
                                    <div class="select">
                                        <select name="type" required>
                                            <option value="s">Leerling</option>
                                            <option value="t">Docent</option>
                                            <option value="c">Klas</option>
                                            <option value="r">Lokaal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Locatie</label>
                                <div class="control">
                                    <div class="select">
                                        <select name="wachtwoord" required>
                                            <option value="Goes">Goes</option>
                                            <option value="GoesNoordhoeklaan">Goes Noordhoeklaan</option>
                                            <option value="GoesStationspark">Goes Stationspark</option>
                                            <option value="KrabbendijkeAppelstraat">Krabbendijke Appelstraat</option>
                                            <option value="KrabbendijkeKerkpolder">Krabbendijke Kerkpolder</option>
                                            <option value="Middelburg">Middelburg</option>
                                            <option value="Tholen">Tholen</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field is-grouped">
                                <div class="control">
                                    <button class="button is-link" type="submit">Inloggen</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <%= HtmlTools.getBasicFooter("WelkomPagina") %>

    </body>
</html>
