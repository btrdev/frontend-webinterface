<%-- 
    Document   : overzicht
    Created on : Sep 7, 2017, 5:11:34 PM
    Author     : Jochem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import = "java.io.*,java.util.*,nl.jochembroekhoff.btr.frontend.webinterface.HtmlTools,nl.jochembroekhoff.btr.frontend.webinterface.SessieTools" %>

<%
    if(!SessieTools.isIngelogd(request.getSession())) {
        response.sendRedirect("./index.jsp");
    }
%>

<!DOCTYPE html>
<html lang="nl">
    <head>
        <%= HtmlTools.getBasicHead("Overzicht") %>
    </head>
    <body>

        <nav class="navbar is-dark">
            <div class="navbar-brand">
                <a class="navbar-item">
                    <%= request.getSession().getAttribute("rooster_code") %>
                </a>
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarContent">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarContent" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="https://calvijn.magister.net/" target="_blank">
                        Magister <span class="icon"><i class="fas fa-external-link-alt"></i></span>
                    </a>
                </div>

                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="field is-grouped">
                            <p class="control">
                                <a class="button is-primary">
                                    <span class="icon">
                                        <i class="fas fa-sign-out-alt"></i>
                                    </span>
                                    <span>
                                        Uitloggen <%-- TODO: Actie --%>
                                    </span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="columns">
                <aside class="column is-one-quarter-desktop" id="sidebar">
                    <p class="menu-label">Roosters</p>
                    <ul class="menu-list">
                        <li><a href="#!/rooster" class="is-active">Lesrooster</a></li>
                        <li><a href="#!/toetsrooster">Toetrsooster</a></li>
                    </ul>
                    <p class="menu-label">Overig</p>
                    <ul class="menu-list">
                        <li><a href="#!/cup">CUP</a></li>
                        <li><a href="#!/info">Info</a></li>
                    </ul>
                </aside>
                <main class="column" role="main">
                    <h1 class="title" id="ui-title">Laden...</h1>
                    
                    <section class="container" id="ui-inhoud">
                        
                    </section>
                </main>
            </div>
        </div>

        <%= HtmlTools.getExtendedFooter() %>

    </body>
</html>
